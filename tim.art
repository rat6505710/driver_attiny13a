; Настраивает таймер для работы в режиме сравнения 
; c генерацией прерывания TIM0_COMPA.
inline proc tim_cmp_init() {
    TCCR0A = tr1 = bitmask(WGM01)
    TIMSK0 = tr1 = bitmask(OCIE0A)
}

; Запускает таймер, настроенный для работы в 
; режиме сравнения с CLK/1.
inline proc tim_cmp_clk_div_1_on() {
    TCCR0B = tr1 = bitmask(CS00)
}

; Запускает таймер, настроенный для работы в 
; режиме сравнения с CLK/8.
inline proc tim_cmp_clk_div_8_on() {
    TCCR0B = tr1 = bitmask(CS01)
}

; Запускает таймер, настроенный для работы в 
; режиме сравнения с CLK/64.
inline proc tim_cmp_clk_div_64_on() {
    TCCR0B = tr1 = bitmask(CS01, CS00)
}

; Запускает таймер, настроенный для работы в 
; режиме сравнения с CLK/256.
inline proc tim_cmp_clk_div_256_on() {
    TCCR0B = tr1 = bitmask(CS02)
}

; Запускает таймер, настроенный для работы в 
; режиме сравнения с CLK/1024.
inline proc tim_cmp_clk_div_1024_on() {
    TCCR0B = tr1 = bitmask(CS02, CS00)
}

; Отключает таймер, настроенный для работы в 
; режиме сравнения.
inline proc tim_cmp_off() {
    TCCR0B = zr               
}

; Сбрасывает счетчик таймера, настроенного 
; для работы в режиме сравнения.
inline proc tim_cmp_reset() {
    TCNT0 = zr               
}

; Устанавливает значение сравнения для таймера, 
; настроенного для работы в режиме сравнения.
inline proc tim_period_set(period: tr1) {
    OCR0A = period          
}
